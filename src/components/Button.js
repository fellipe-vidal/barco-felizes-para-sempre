import styled from 'styled-components'

const Button = styled.button`

  display: block;
  font-family: "Montserrat", sans-serif;
  font-weight: 600;
  color: #333;
  font-size: 0.95rem;
  background-color: ${ ({color}) => color };
  border: none;
  padding: 15px 30px;
  border-radius: 4px;
  margin: ${ ({center}) => center ? "0 auto" : "0" };
  box-shadow: 0 1px 2px rgba( 0, 0, 0, .25 );
  cursor: pointer;

`

export default Button