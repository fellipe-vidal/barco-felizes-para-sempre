import styled from 'styled-components'

const getWindowPosition = () => {
  return parseInt(window.scrollY)
}

const ModalContainer = styled.div`

  display: ${({visible}) => visible ? "block" : "none" };
  position: absolute;
  z-index: 10000;
  left: 50%;
  transform: translateX(-50%);
  top: calc(${() => getWindowPosition()}px + ${window.innerHeight * 0.05}px);
  height: ${window.innerHeight * 0.9}px;
  width: calc(${(window.innerWidth * 0.9)}px - 1em);
  max-width: 960px;
  background-color: white;
  border-radius: 4px;
  box-shadow: 0 4px 12px rgba( 0, 0, 0, .25 );
  overflow-y: scroll;
  overflow-x: hidden;

`

export default ModalContainer;