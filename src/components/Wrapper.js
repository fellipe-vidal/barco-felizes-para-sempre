import styled from 'styled-components'

const Wrapper = styled.div`

  max-width: 960px;
  margin: 0 auto;

`

export default Wrapper;