import React from 'react'
import Slick from 'react-slick'

import Container from './components/Container'
import QuoteCard from './quote-card'
import Wrapper from '../../components/Wrapper'

const settings = {
  arrows: false,
  autoplay: true,
  autoplaySpeed: 4000,
  dots: false,
  speed: 800,
  slidesToScroll: 1,
  responsive: [{
    breakpoint: 375,
    settings: {
      slidesToShow: 1
    }},
    {
    breakpoint: 768,
    settings: {
      slidesToShow: 2
    }},
    {
    breakpoint: 10000,
    settings: {
      slidesToShow: 3
    }
  }]
}

const Testimonials = () => {

  return (
    <Container>
      <Wrapper>
        <Slick {...settings} style={{ alignItems: "center" }} >
          <QuoteCard content="Foi maravilhosa a experiência e o atendimento! Com certeza esse será um passeio indicado à todos os clientes da nossa agência! Obrigada!" author="AV Tour" />
          <QuoteCard content="Momentos mais do que especiais!❤ Bom demais!!!" author="Fátima Kreimer" />
          <QuoteCard content="Esse barco é incrível! E o astral dos donos, insuperáveis! Vou repetir." author="Carolina Madrid" />
          <QuoteCard content="Melhor passeio de barco que já fiz, anfritriões mais lindos e simpáticos." author="Eliane Santos" />
        </Slick>
      </Wrapper>
    </Container>
  )
}

export default Testimonials
