import React from 'react'

import Container from './components/Container'
import QuoteIcon from './components/QuoteIcon'
import QuoteText from './components/QuoteText'
import QuoteAthor from './components/QuoteAuthor'

const QuoteCard = ({ content, author }) => {
  return (
    <Container>
      <QuoteIcon />
      <QuoteText>
        {content}
      </QuoteText>
      <QuoteAthor>
        {author}
      </QuoteAthor>
    </Container>
  )
}

export default QuoteCard
