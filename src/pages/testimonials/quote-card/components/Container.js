import styled from 'styled-components'

const Container = styled.div`

  display: grid;
  position: relative;
  background-color: #FEFEFE;
  border-radius: 4px;
  padding: 3em;
  box-shadow: 0 1px 2px rgba( 0, 0, 0, .25 );
  height: 15em;
  justify-content: center;
  align-items: center;
  margin: 0 .5em 2em;

`

export default Container;