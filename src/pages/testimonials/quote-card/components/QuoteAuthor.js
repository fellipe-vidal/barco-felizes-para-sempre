import styled from 'styled-components'

const QuoteAuthor = styled.p`

  font-family: Montserrat;
  font-weight: 600;
  font-size: .80em;
  text-align: right;
  color: #444;

  &:before {
    content: "- ";
    font-family: Montserrat;
    font-weight: 600;
    color: #444;
  }


`

export default QuoteAuthor;