import styled from 'styled-components'

const QuoteText = styled.p`

  font-family: Montserrat;
  font-size: .9em;
  color: #121212;
  font-weight: 300;
  line-height: 1.5em;
  margin-bottom: 1em;

`

export default QuoteText;