import styled from 'styled-components'
import img from '../../../../assets/icons/quote-icon.svg'

const QuoteIcon = styled.span`

  position: absolute;
  background-image: url(${img});
  background-size: cover;
  width: 1.1em;
  height: 1.1em;
  left: 1.3em;
  top: 1.3em;

`

export default QuoteIcon;