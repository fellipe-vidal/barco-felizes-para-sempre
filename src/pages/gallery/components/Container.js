import styled from 'styled-components'

const Container = styled.div`

  position: relative;
  height: ${window.innerHeight * 0.5}px;

  @media (min-width: 769px) {
    height: ${window.innerHeight * 0.8}px;
  }

`

export default Container;