import styled from 'styled-components'

const Image = styled.div`

  height: ${window.innerHeight * 0.5}px;
  background-image: url(${({src}) => src});
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  padding: 0;
  margin: 0;

  @media (min-width: 769px) {
    height: ${window.innerHeight * 0.8}px;
  }

`

export default Image;