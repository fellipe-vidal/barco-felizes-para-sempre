import React from 'react'

import Container from './components/Container'
import Arrow from './components/Arrow'

import left from '../../../../assets/icons/left.svg'
import right from '../../../../assets/icons/right.svg'

const Arrows = ({previous, next}) => {
  return (
    <Container>
      <Arrow icon={left} onClick={previous} />
      <Arrow icon={right} onClick={next} />
    </Container>
  )
}

export default Arrows
