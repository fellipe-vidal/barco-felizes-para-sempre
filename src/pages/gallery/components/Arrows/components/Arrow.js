import styled from 'styled-components'

const Arrow = styled.div`

  height: 1.5em;
  width: 1.5em;
  background-image: url(${({icon}) => icon});
  background-size: contain;
  background-position: center;
  cursor: pointer;

`

export default Arrow;