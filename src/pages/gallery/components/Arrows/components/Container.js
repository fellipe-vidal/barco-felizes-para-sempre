import styled from 'styled-components'

const Container = styled.div`

  display: flex;
  justify-content: space-between;
  position: absolute;
  z-index: 1000;
  width: 100%;
  height: 100%;
  max-width: 960px;
  padding: 1em;
  align-items: center;
  box-sizing: border-box;

`

export default Container;