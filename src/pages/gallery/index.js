import React, {createRef} from 'react'
import Slick from 'react-slick'

import Container from './components/Container'
import Image from './components/Image'
import Arrows from './components/Arrows/index'
import Wrapper from '../../components/Wrapper'

import img1 from '../../assets/imgs/img_1.jpg'
import img2 from '../../assets/imgs/img_2.jpg'
import img3 from '../../assets/imgs/img_3.jpg'
import img4 from '../../assets/imgs/img_4.jpg'
import img5 from '../../assets/imgs/img_5.jpg'
import img6 from '../../assets/imgs/img_6.jpg'
import img7 from '../../assets/imgs/img_7.jpeg'
import img8 from '../../assets/imgs/img_8.jpeg'
import img9 from '../../assets/imgs/img_9.jpeg'
import img10 from '../../assets/imgs/img_10.jpeg'
import img11 from '../../assets/imgs/img_11.jpeg'
import img12 from '../../assets/imgs/img_12.jpeg'
import img13 from '../../assets/imgs/img_13.jpeg'
import img14 from '../../assets/imgs/img_14.jpeg'
import img15 from '../../assets/imgs/img_15.jpeg'
import img16 from '../../assets/imgs/img_16.jpeg'
import img17 from '../../assets/imgs/img_17.jpeg'

const settings = {
  autoplay: true,
  arrows: true,
  autoplaySpeed: 4000,
  speed: 1000,
  // fade: true,
  dots: false,
  infinite: true,
  
}

const previous = () => {
  ref.current.slickPrev();
}

const next = () => {
  ref.current.slickNext()
}

const ref = createRef();

const Gallery = () => {
  return (
    <Container>
      <Wrapper>
        <Arrows next={next} previous={previous} />
      </Wrapper>
      <Slick ref={ref} {...settings}>
          <Image src={img1} />
          <Image src={img2} />
          <Image src={img3} />
          <Image src={img4} />
          <Image src={img5} />
          <Image src={img6} />
          <Image src={img7} />
          <Image src={img8} />
          <Image src={img9} />
          <Image src={img10} />
          <Image src={img11} />
          <Image src={img12} />
          <Image src={img13} />
          <Image src={img14} />
          <Image src={img15} />
          <Image src={img16} />
          <Image src={img17} />
      </Slick>
    </Container>
  )
}

export default Gallery
