import React from 'react'

import Logo from './components/Logo'
import Container from './components/Container'


const Header = () => {
  return (
    <Container >
      <Logo>Barco Felizes Para Sempre</Logo>
    </Container>
  )
}

export default Header
