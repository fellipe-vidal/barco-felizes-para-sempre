import styled from 'styled-components'

const Container = styled.div`
  padding: 10px;
  background-color: #3A68AD;
  display: flex;
  justify-content: center;
  align-content: center;
  box-shadow: 0 2px 4px rgba( 0, 0, 0, 0.25 );
`

export default Container