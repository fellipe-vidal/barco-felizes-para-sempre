import styled from 'styled-components'

const Logo = styled.h1`

  font-size: 1.6rem;
  font-family: 'Pacifico', sans-serif;
  color: #FFF;

`

export default Logo;