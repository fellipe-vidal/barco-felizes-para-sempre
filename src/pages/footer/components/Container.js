import styled from 'styled-components'

const Container = styled.div`

  background-color: #2F548C;
  padding: 2em 0;
  color: white;
  text-align: center;
  font-family: Montserrat, sans-serif;
  font-size: .7em;


`

export default Container;