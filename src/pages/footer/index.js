import React from 'react'

import Container from './components/Container'

const Footer = () => {
  return (
    <Container>
      Copyright  © 2020 - Barco Felizes Para Sempre
    </Container>
  )
}

export default Footer
