import styled from 'styled-components'

const Title = styled.h3`

  font-family: "Montserrat", sans-serif;
  font-weigh: bold;
  text-align: center;
  color: #214780;
  margin: 0 auto 30px;

`

export default Title;