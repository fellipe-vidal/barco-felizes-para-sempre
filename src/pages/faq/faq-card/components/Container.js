import styled from 'styled-components'

const Container = styled.div`

display: grid;
  width: 100%;
  padding: 50px 40px;
  box-sizing: border-box;
  align-items: center;
  justify-content: center;

`

export default Container;