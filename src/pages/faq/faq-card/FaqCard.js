import React, { useContext } from 'react'
import { Context } from '../../../context/Provider'
import { toggleFaqModal } from '../../../context/actions'

import Container from './components/Container'
import Title from './components/Title'
import Button from '../../../components/Button'

const FaqCard = () => {

  const { state, dispatch } = useContext(Context)

  return (
    <Container>
      <Title>Saiba Tudo Sobre Seu Passeio</Title>
      <Button center color="#61D4B3" onClick={() => dispatch(toggleFaqModal())} >Esclareça Suas Dúvidas</Button>
    </Container>
  )
}

export default FaqCard
