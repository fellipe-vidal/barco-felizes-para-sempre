import React from 'react'

import FaqCard from './faq-card/FaqCard'
import Modal from './modal/index'
import Wrapper from '../../components/Wrapper'

const Faq = () => {

  return (
    <>
      <Wrapper>
        <FaqCard />
        <Modal />
      </Wrapper>
    </>
  )
}

export default Faq
