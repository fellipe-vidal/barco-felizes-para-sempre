import styled from 'styled-components'

const Text = styled.p`

  margin: 0 1em 2em ;
  font-family: Montserrat, sans-serif;
  font-size: 0.9em;
  line-height: 1.4em;
  color: #333;

`

export default Text;