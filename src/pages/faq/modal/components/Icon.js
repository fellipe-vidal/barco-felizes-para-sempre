import styled from 'styled-components'

const Icon = styled.div`

  display: block;
  height: 2em;
  width: 2em;
  position: sticky;
  background-image: url(${({icon}) => icon});
  background-color: white;
  border-radius: 50%;
  border: 2px solid black;
  background-size: 1.5em;
  background-repeat: no-repeat;
  background-position: center;
  left: 100%;
  top: 1em;
  cursor: pointer;

`

export default Icon;
