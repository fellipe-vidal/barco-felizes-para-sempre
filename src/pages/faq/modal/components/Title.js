import styled from 'styled-components'

const Title = styled.h4`

  margin: 0 1em 1em;
  font-family: Montserrat, sans-serif;
  font-size: 1.1em;
  color: #121212;

`

export default Title;