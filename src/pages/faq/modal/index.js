import React, { useContext } from 'react'
import { Context } from '../../../context/Provider'
import { toggleFaqModal } from '../../../context/actions'

import ModalContainer from '../../../components/ModalContainer'
import Title from './components/Title'
import Text from './components/Text'
import Icon from './components/Icon'

import close from '../../../assets/icons/close.svg'

const Modal = ({ visible }) => {

  const { state, dispatch } = useContext(Context)

  return (
    <ModalContainer visible={state.isFaqModalOpen} >
      <Icon icon={close} onClick={() => dispatch(toggleFaqModal())} />
      <Title>
        Como funciona a locação do Barco Felizes para Sempre?
      </Title>
      <Text>
        O Barco Felizes para Sempre não cobra passeio de forma individual por passageiro.

        A locação do Barco Felizes para Sempre é feita diretamente a uma pessoa, a qual fica responsável pelo seu grupo.

        Está incluso na locação, o marinheiro e o combustível.

        Período mínimo de locação 1 (uma) hora.

        Para a confirmação da locação e garantia de data, é necessário fazer o pagamento de 50% (cinquenta por cento)  do valor da locação pretendida e assinar o contrato de locação.

        Na hora do embarque, a pessoa responsável pelo contrato de locação, deverá entregar ao marinheiro do Barco Felizes para Sempre, os Termos de Responsabilidades preenchidos por todos os convidados, e o saldo restante, referente ao contrato de locação deve estar quitado, caso contrário, o barco não saíra do pier.

        O Barco Felizes para Sempre tem capacidade para 24 passageiros. (bebê de colo conta como um passageiro)
      </Text>
      <Title>
        O que é o material de salvatagem que é exigido pela Marinha?
      </Title>
      <Text>
        Coletes salva-vidas de acordo com a capacidade da embarcação.
        Luzes de navegação.
        Extintor de incêndio.
  
        O Barco Felizes para Sempre está sempre em dia com todo o material de salvatagem exigido pela Marinha.
      </Text>
      <Title>
        Qual estrutura o Barco Felizes para Sempre oferece aos seus clientes? 
      </Title>
      <Text>
        O Barco Felizes para Sempre possui dois decks. No deck inferior, tem banheiro, copa com pia, duas mesas, caixa térmica, duas caixas de som, bancos para 20 pessoas e churrasqueira. No deck superior tem uma confortável área com bancos e mesa embaixo de uma cobertura e espaço aberto para tomar sol.
      </Text>
      <Title>
        O Barco Felizes para Sempre disponibiliza alimentação e bebidas durante o período de locação?
      </Title>
      <Text>
       Não. Toda alimentação e bebidas levadas a bordo durante o período de locação é de responsabilidade do locatário. 
      </Text>
      <Title>
        O que levar para o Barco então?
      </Title>
      <Text>
      Cada um leva o que quiser beber e comer, como água, sucos, cervejas, espumantes, vinhos, salgadinhos, petiscos, mesas de frios, etc.  
      Observação: É necessário levar gelo para gelar as bebidas.
      </Text>

    </ModalContainer>
  )
}

export default Modal
