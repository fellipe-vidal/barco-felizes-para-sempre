import styled from 'styled-components'

const Container = styled.div`

  background: transparent linear-gradient(270deg, #FFFFFF00 0%, #0000004F 31%, #000000A8 100%) 0% 0% no-repeat padding-box;
  
  padding: 3em 1em;

`

export default Container