import styled from 'styled-components'

const Title = styled.h2`

  font-family: "Montserrat", sans-serif;
  font-weight: 800;
  color: #FFF;
  max-width: 70%;
  font-size: 1.3rem;
  margin-bottom: 15px

`

export default Title