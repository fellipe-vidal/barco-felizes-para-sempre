import styled from 'styled-components'

import hero from '../../../assets/imgs/hero.jpeg'

const BgImage = styled.div`

  background-image: url(${hero});
  background-size: cover;
  background-position: center;


`

export default BgImage;