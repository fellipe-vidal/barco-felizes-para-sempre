import styled from 'styled-components'

const Subtitle = styled.p`

  font-family: "Montserrat", sans-serif;
  font-weight: 300;
  color: #FFF;
  max-width: 70%;
  font-size: 0.9 rem;
  margin-bottom: 35px

`

export default Subtitle;