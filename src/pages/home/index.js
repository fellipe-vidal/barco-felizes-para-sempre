import React from 'react'
import { Link } from 'react-scroll';

import Container from './components/Container'
import BgImage from './components/BgImage'
import Title from './components/Title'
import Subtitle from './components/Subtitle'
import Button from '../../components/Button'
import Wrapper from '../../components/Wrapper'

const Home = () => {
  return (
    <BgImage>
      <Container>
        <Wrapper>
          <Title>Desfrute do melhor passeio de Brasília</Title>
          <Subtitle>Passeios diurnos e noturnos, para que você possa ter uma experiência inesquecível</Subtitle>
          <Link
            activeClass="active"
            to="contact"
            spy={true}
            smooth={true}
            offset={20}
            duration={800}
          >
            <Button color="#F09595">Garanta seu passeio</Button>
          </Link>
        </Wrapper>
      </Container>
    </BgImage>
  )
}

export default Home
