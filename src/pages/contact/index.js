import React from 'react'

import Container from './components/Container'
import Title from './components/Title'
import ContactButton from './components/contact-button/index'

import instagram from '../../assets/icons/instagram-icon.svg'
import whatsapp from '../../assets/icons/whatsapp-icon.svg'
import email from '../../assets/icons/email-icon.svg'

const openInstagram = () => {
  const url = "https://www.instagram.com/barcofelizespsempre/"
  window.open(url, '_blank');
}

const openWhatsapp = () => {
  const url = "https://api.whatsapp.com/send?phone=5561991151523"
  window.open(url, '_blank');
}

const openMail = () => {
  const url = "mailto:contato@barcofelizesparasempre.com.br"
  window.open(url, 'emailWindow')
}

const Contact = () => {
  return (
    <Container>
      <Title>Agende Agora Seu Passeio</Title>
      <ContactButton color="#C13584" icon={instagram} title="Instagram" onClick={openInstagram} />
      <ContactButton color="#25D366" icon={whatsapp} title="Whatsapp" onClick={openWhatsapp} />
      <ContactButton color="#EA4335" icon={email} title="Email" onClick={openMail} />
    </Container>
  )
}

export default Contact
