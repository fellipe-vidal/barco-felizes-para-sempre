import React from 'react'

import Container from './components/Container'
import Icon from './components/Icon'
import Title from './components/Title'

const ContactButton = ({title, icon, color, onClick}) => {
  return (
    <Container color={color} onClick={onClick} >
      <Icon src={icon} />
      <Title>
        {title}
      </Title>
    </Container>
  )
}

export default ContactButton
