import styled from 'styled-components'

const Icon = styled.div`

  width: 1.2em;
  height: 1.2em;
  margin-right: 1em;
  background-image: url(${({src}) => src});
  background-size: contain;
  background-repeat: no-repeat;
  background-position: center;

`

export default Icon;