import styled from 'styled-components'

const Title = styled.h3`

  font-family: Montserrat, sans-serif;
  color: white;
  font-size: 1em;
  font-weight: 600;
  text-align: center;

`

export default Title;