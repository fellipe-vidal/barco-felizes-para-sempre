import styled from 'styled-components'

const Container = styled.div`

  display: flex;
  background-color: ${({color}) => color};
  margin: 0 auto 1em;
  padding: 1em 3em;
  justify-content: center;
  align-items: center;
  max-width: 12em;
  border-radius: 4px;
  box-shadow: 0 1px 2px rgba( 0, 0, 0, 0.25 );
  cursor: pointer;

  &:last-child {
    margin-bottom: 0;
  }

`

export default Container;