import { 
  TOGGLE_EMAIL_MODAL,
  TOGGLE_FAQ_MODAL,
  TOGGLE_TESTIMONIAL_MODAL,
  GET_DEVICE
} from './actions'

const ContextReducer = (state, action) => {

  switch (action.type){
    case TOGGLE_EMAIL_MODAL:
      return {...state, isEmailModalOpen: !state.isEmailModalOpen};

    case TOGGLE_FAQ_MODAL:
      return {...state, isFaqModalOpen: !state.isFaqModalOpen};

    case TOGGLE_TESTIMONIAL_MODAL:
      return {...state, isTestimonialModalOpen: !state.isTestimonialModalOpen};

    case GET_DEVICE:
      return {...state, device: action.device };

    default:
      return state;
  }

}

export default ContextReducer;