export const TOGGLE_EMAIL_MODAL = "TOGGLE_EMAIL_MODAL"
export const TOGGLE_FAQ_MODAL = "TOGGLE_FAQ_MODAL"
export const TOGGLE_TESTIMONIAL_MODAL = "TOGGLE_TESTIMONIAL_MODAL"
export const GET_DEVICE = "GET_DEVICE"

export const toggleEmailModal = () => {
  return {type: TOGGLE_EMAIL_MODAL}
}

export const toggleFaqModal = () => {
  return {type: TOGGLE_FAQ_MODAL}
}

export const toggleTestimonialModal = () => {
  return {type: TOGGLE_TESTIMONIAL_MODAL}
}

export const getDevice = (deviceWidth) => {
  let device = "phone"
  if (deviceWidth > 768) {
    device = "dektop"
  }
  if (deviceWidth > 375) {
    device = "tablet"
  }

  return {type: GET_DEVICE, device}
}
