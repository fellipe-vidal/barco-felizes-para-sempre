import React, { createContext, useReducer } from 'react'
import ContextReducer from './reducer'

export const Context = createContext();

const initialState = {
  isEmailModalOpen: false,
  isFaqModalOpen: false,
  isTestimonialModalOpen: false,
  device: "phone"
}

const ContextProvider = (props) => {

  const [ state, dispatch ] = useReducer(ContextReducer, initialState)

  return (
    <Context.Provider value={{state, dispatch}} >
      {props.children}
    </Context.Provider>
  )
}

export default ContextProvider
