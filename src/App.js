import React from 'react'
import { Element } from 'react-scroll'

import Header from './pages/header/index'
import Home from './pages/home/index'
import Faq from './pages/faq/index'
import Gallery from './pages/gallery/index'
import Testimonials from './pages/testimonials/index'
import Contact from './pages/contact/index'
import Footer from './pages/footer/index'

import ContextProvider from './context/Provider'

const App = () => {

  return (
    <ContextProvider>
      <Header />
      <Home />
      <Faq />
      <Gallery />
      <Testimonials />
      <Element  name="contact">
        <Contact />
      </Element>
      <Footer />
    </ContextProvider>
  )

}

export default App
